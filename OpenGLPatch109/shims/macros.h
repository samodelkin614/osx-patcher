#include <stdint.h>

#define make_return_zero(func) int func(void) { return 0; }
#define make_empty(func) void func(void) { }
