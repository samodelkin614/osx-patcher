#include "macros.h"
#include <CoreFoundation/CoreFoundation.h>

/*
Adds stubs for functions that libGFXShared.dylib added in 10.8-10.9
*/

// these are used in OpenCL!
make_return_zero(gfxAnnotateAddDataSource)
make_return_zero(gfxAnnotateBuffer)
make_empty(gfxAnnotateRemoveDataSource)
make_return_zero(gfxAnnotateTexture)
make_return_zero(gfxGetGFXBufferForGFXTexture) // used in opencl but ONLY for gpu not cpu

// these will be reexported from 10.9 lib
// gfxFindIOFramebufferWithOpenGLDisplayMask
// gfxTextureLevelBytes

CFArrayRef gfxGetOGLMemoryAnnotationList(void) {
	return CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks); // it HAS to be mutable!
}
