#!/bin/bash
cp libGL_ml.dylib libGL_ml_p.dylib
fat_offset64=`otool -f libGL_ml.dylib | grep -m2 offset | tail -n1 | awk '{print $2}'`
for a in glGetObjectLabelEXT glInsertEventMarkerEXT glLabelObjectEXT glPopGroupMarkerEXT glPushGroupMarkerEXT; do printf '\xc3' | dd of=libGL_ml_p.dylib bs=1 count=1 conv=notrunc seek=$((0x$(otool -tvV libGL_ml.dylib| grep -A1 "_$a:$" | tail -n1 | cut -d "`printf '\t'`" -f1)+$fat_offset64)); done
fat_offset32=`otool -f libGL_ml.dylib | grep -m1 offset | awk '{print $2}'`
for a in glGetObjectLabelEXT glInsertEventMarkerEXT glLabelObjectEXT glPopGroupMarkerEXT glPushGroupMarkerEXT; do printf '\xc3' | dd of=libGL_ml_p.dylib bs=1 count=1 conv=notrunc seek=$((0x$(otool -arch i386 -tvV libGL_ml.dylib| grep -A1 "_$a:$" | tail -n1 | cut -d "`printf '\t'`" -f1)+$fat_offset32)); done
rm -f libGL_ml_p.dylib.unsigned
unsign/unsign libGL_ml_p.dylib
rm -f libGL_ml_p.dylib 
mv libGL_ml_p.dylib.unsigned libGL_ml_p.dylib
mv libGL_ml_p.dylib OpenGL.framework/Versions/A/Libraries/libGL.dylib
